# 《Go语言设计模式》配套代码
#### 《Go语言设计模式》随书代码开源啦～ 欢迎喜欢的朋友多多 star,fork~
# 代码安装步骤如下：
#### 安装步骤如下：
#### （1）下载源码；
#### （2）解压；
#### （3）将解压的文件夹gitee.com或者github.com复制到$GOPATH/src目录下，如果gitee.com或者github.com目录已经存在，请将子目录shirdonl下的文件夹复制到gitee.com或者github.com目录中。

# 图书展示
<img src="bookInfo/bookPic1.png" width = "400" height = "480" alt="图片名称" align=center />

# 图书促销活动中，欢迎点击如下链接抢购：
https://item.jd.com/13847234.html

![qrcode](bookInfo/codebigdata.jpg)
#### 假如读者在阅读本书的过程中有任何疑问，请关注“源码大数据”公众号，并按照提示输入读者的问题，作者会尽快进行交流回复。







